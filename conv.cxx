#include "NSWConverter.h"

int main(void){

  std::string inputdir = "/home/okesaku/software/DB_LUT/hibi/TGCNSW_CW_DB_pT20_10/";
  std::string outputdir = "/home/okesaku/software/DB_LUT/hibi/TGCNSW_CW_DB_pT20_10_address/";
  
  if(mkdir(outputdir.c_str(), 0777) == 0){
    std::cout << "[INFO] Directry " << outputdir 
	      << " is created" << std::endl;
  }else{
    std::cout << "[WARNING] Directry " << outputdir 
	      << " is NOT created" << std::endl;
  }
  
  NSWConverter nsw; 
  //nsw.Convert(inputdir, outputdir);
  //nsw.MakeTestLUT(outputdir);
  nsw.MakeCoeFile(outputdir);

  return 0;
}

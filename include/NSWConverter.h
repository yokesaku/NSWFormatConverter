#ifndef _NSWConverter_h_
#define _NSWConverter_h_

#include "DBFileConverter.h"

class NSWConverter : public DBFileConverter
{

 public:
  void Convert(std::string input, std::string output);
  void ReadWriteFunction_EtaPhi(const std::string &inputfilename, const std::string &outputfilename);
  void ReadWriteFunction_EtaDtheta(const std::string &inputfilename, const std::string &outputfilename);
  void MakeTestLUT(const std::string &output);
  void MakeCoeFile(const std::string &outputdir);
  int  TestPatternPos(int deta, int dphi);
  int  TestPatternAng(int deta, int dphi);

};

#endif //_NSWConverter_h_

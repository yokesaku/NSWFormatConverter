# Process name (same as .cxx file of main())
ProcName := conv

JP_TARGET := $(ProcName)
JP_LIB := conv

ifndef JP_WORKDIR
  JP_WORKDIR = $(PWD)
endif

# Toggle variables (positional qualifiers) ...
OUT_OBJ := -o 
FOR_OBJ := -o 
OUT_LIB := 
OUT_EXE := -o 
LIB_PATH := -L
PATH_DEL := /

# pattern for next substitution
LIB_PATT := -l$(LibName)
FLIB_PATT := $(LIB_PATT)

CPPFLAGS += -Iinclude

ifndef LDFLAGS
  LDFLAGS :=
endif

CXX       := g++
CXXFLAGS  := -W -Wall -ansi -pedantic -Wno-non-virtual-dtor -Wno-long-long
CXXFLAGS  += -Wwrite-strings -Wpointer-arith -Woverloaded-virtual -pipe

# OPTIMIZE
CXXFLAGS  += -O2
CCFLAGS   := -O2

# for Shared library
CXXFLAGS  += -fPIC
CCFLAGS   += -fPIC

CC      := gcc
SHEXT   := so

ifndef JP_TMPDIR
  JP_TMPDIR := $(JP_WORKDIR)/tmp
endif
DUMMY_VARIABLE:=$(shell [ ! -d $(JP_TMPDIR) ] && mkdir -p $(JP_TMPDIR) )
ifndef JP_LIBDIR
  JP_LIBDIR := $(JP_WORKDIR)/lib
endif
ifndef JP_BINDIR
  JP_BINDIR := $(JP_WORKDIR)/bin
endif

# // ===========================================================
# // implicit rules (default)
# // ===========================================================

%.o: %.cxx
	@$(CXX) -c -o $@ $(CXXFLAGS) $(CPPFLAGS) $*.cxx

# // ===========================================================
# // build
# // ===========================================================

$(JP_TMPDIR)/%.d: src/%.cxx
	@if [ ! -d $(JP_TMPDIR) ]; then mkdir $(JP_TMPDIR) ;fi
	@echo Making dependency for file $< ...
	@$(CXX) -MM $(CXXFLAGS) $(CPPFLAGS) $<  > $@
	@sed -e "1 s/^/tmp\//g" $@ > $(JP_TMPDIR)/.tmp
	@\mv $(JP_TMPDIR)/.tmp $@
	@touch $@

zzz := $(patsubst src%, $(JP_TMPDIR)%, $(wildcard src/*.cxx))
Zdependencies := $(patsubst %.cxx, %.d, $(zzz))
Zheaders := $(wildcard include/*.h)
Zsources := $(wildcard src/*.cxx)
Zobjects := $(patsubst %.cxx, %.o, $(zzz))

$(JP_TMPDIR)/%.o: src/%.cxx
	@echo Compiling src/$*.cxx ...
	@$(CXX) -c -o $@ $(CXXFLAG) $(CPPFLAGS) src/$*.cxx

$(JP_TMPDIR)/%.d: %.cxx
	@if [ ! -d $(JP_TMPDIR) ] ; then mkdir $(JP_TMPDIR) ;fi
	@echo Making dependency for file $< ...
	@$(CXX) -MM $(CXXFLAGS) $(CPPFLAGS) $<  > $@
	@sed -e "1 s/^/tmp\//g" $@ > $(JP_TMPDIR)/.tmp
	@\mv $(JP_TMPDIR)/.tmp $@
	@touch $@

dependencies :=$(patsubst %.cxx, $(JP_TMPDIR)/%.d, $(wildcard *.cxx))
headers := $(wildcard include/*.h)
sources := $(wildcard *.cxx)
objects := $(patsubst %.cxx,$(JP_TMPDIR)/%.o, $(wildcard *.cxx))

$(JP_TMPDIR)/%.o: %.cxx
	@echo Compiling $*.cxx ... 
	@$(CXX) -c -o $@ $(CXXFLAGS) $(CPPFLAGS) $*.cxx

.PHONY: clean

$(JP_BINDIR)/$(JP_TARGET): $(objects) $(JP_LIBDIR)/lib$(JP_LIB).a
	@echo Making $@ ...
	@if [ ! -d $(JP_BINDIR) ]; then mkdir $(JP_BINDIR) ;fi
	@$(CXX) -o $@ $(objects) lib/lib$(JP_LIB).a $(LDFLAGS) $(LOADLIBS)

$(JP_LIBDIR)/lib$(JP_LIB).a: $(Zobjects)
	@echo Making $@ ...
	@if [ ! -d $(JP_LIBDIR) ]; then mkdir $(JP_LIBDIR) ;fi
	@ar r $@ $(Zobjects)
	@ranlib $@

clean:
	@echo Cleaning up ...
	@rm -f ./core
	@rm -rf $(JP_TMPDIR)
	@rm -rf $(JP_LIBDIR)
	@rm -rf $(JP_BINDIR)
	#@rm -rf $(JP_LIBDIR)/lib$(JP_LIB).a
	#@rm -rf $(JP_BINDIR)/$(JP_TARGET)

$(dependencies) : $(sources) $(headers)
	-include $(dependencies)

$(Zdependencies) : $(Zsources) $(Zheaders)
	-include $(Zdependencies)



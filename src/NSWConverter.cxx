#include <stdint.h>
#include <iomanip>
#include <fstream>
#include <sstream>
#include "NSWConverter.h"

void NSWConverter::Convert(std::string inputdir, std::string outputdir){
  std::string kSide[2] = {"c", "a"};
  std::string kModule[12] = {"0a", "1a", "3a", "4a", "6a", "7a", "2a", "5b", "8a", "2b", "5a", "8b"}; 
  std::string kType[2] = {"EtaDtheta", "EtaPhi"};
  int kRoI[2] = {148, 64};

  for(int side=0; side<2; side++){
    for(int type=0; type<2; type++){
      for(int mod=0; mod<12; mod++){
	  std::stringstream ss_input;
	  ss_input << "cm_" << kSide[side] << kModule[mod] << kType[type] << "_0030.db";
	  std::string inputname = inputdir + ss_input.str();
	  
	  std::stringstream ss_output;
	  ss_output << "sl_" << kSide[side] << kModule[mod] << kType[type] << "_0030_LUTconf.txt";
	  std::string outputname = outputdir + ss_output.str();
	  if(type == 1) ReadWriteFunction_EtaPhi(inputname, outputname);
	  else		ReadWriteFunction_EtaDtheta(inputname, outputname);
      }
    }
  }
  return;  
}

void NSWConverter::ReadWriteFunction_EtaPhi(const std::string &inputfilename, const std::string &outputfilename){
	std::cout << inputfilename << std::endl;
	std::cout << "output:   " << outputfilename << std::endl;
	
	std::ifstream inputfile;
	inputfile.open((inputfilename).c_str());
	if(inputfile.fail()) return;
	
	int address;
	int roi;
	int pt[19][8192] = {};
	char NON;
	int tmp;
	int checkEta;
	int checkPhi;
	int deta = 0;
	int SSC = 0;
	
	std::string line;
	while(getline(inputfile, line)){
	  std::istringstream ss_line(line);
	  if(line[0] == '#'){
	    ss_line >> NON >> std::dec >> roi >> checkEta >> checkPhi;
	  }else{
	    SSC = (roi < 4) ? 0 : (roi + 4)/8;
	    int posR = (roi >> 2) & 0x1;
	    int posPhi = roi & 0x3;
	    for(int dphi = 0; dphi < checkPhi; dphi++){
	      address = deta + dphi*64 + posPhi*1024 + posR*4096;
	      ss_line >> std::hex >> pt[SSC][address];
	    }

	    if(deta == 63)
	      deta = 0;
	    else
	      deta++;
	  }
	}

	inputfile.close();
	
	int pt_tot[19*8192] = {};
	int cnt_add0 = 0;
	std::ofstream outputfile((outputfilename).c_str());
	for(int ssc=0;ssc<19;ssc++){
	      for(int address=0; address<8192; address++){
		pt_tot[ssc*8192 + address] = pt[ssc][address];
	      }
	}

	for(int i=0;i<19*8192;i+=4){
		int data0 = pt_tot[i    ];
		int data1 = pt_tot[i + 1];
		int data2 = pt_tot[i + 2];
		int data3 = pt_tot[i + 3];
	      	uint16_t tmp = (data3 << 12) | (data2 << 8) | (data1 << 4) | data0;
		outputfile << "1 : 0x" << std::hex << std::setfill('0') << std::setw(4) << tmp << std::endl;
		cnt_add0++;
	}

	std::cerr << cnt_add0 << std::endl;
	
	outputfile.close();
	  return;  
}

void NSWConverter::ReadWriteFunction_EtaDtheta(const std::string &inputfilename, const std::string &outputfilename){
	std::cout << inputfilename << std::endl;
	std::cout << "output:   " << outputfilename << std::endl;
	
	std::ifstream inputfile;
	inputfile.open((inputfilename).c_str());
	if(inputfile.fail()) return;
	
	int address;
	int roi;
	int pt[19][16384] = {};
	char NON;
	int tmp;
	int checkEta;
	int checkTheta;
	int deta = 0;
	int SSC = 0;
	
	std::string line;
	while(getline(inputfile, line)){
	  std::istringstream ss_line(line);
	  if(line[0] == '#'){
	    ss_line >> NON >> std::dec >> roi >> checkEta >> checkTheta;
	  }else{
	    SSC = (roi < 4) ? 0 : (roi + 4)/8;
	    int posR = (roi >> 2) & 0x1;
	    int posPhi = roi & 0x3;
	    for(int dtheta = 0; dtheta < checkTheta; dtheta++){
	      address = deta + dtheta*64 + posPhi*2048 + posR*8192;
	      ss_line >> std::hex >> pt[SSC][address];
	    }

	    if(deta == 63)
	      deta = 0;
	    else
	      deta++;

	  }
	}

	inputfile.close();

	int pt_tot[19*16384] = {};
	int cnt_add0 = 0;
	std::ofstream outputfile((outputfilename).c_str());
	for(int ssc=0;ssc<19;ssc++){
	      for(int address=0; address<16384; address++){
		pt_tot[ssc*16384 + address] = pt[ssc][address];
	      }
	}
	
	for(int i=0;i<19*16384;i+=4){
		int data0 = pt_tot[i    ];
		int data1 = pt_tot[i + 1];
		int data2 = pt_tot[i + 2];
		int data3 = pt_tot[i + 3];
	      	uint16_t tmp = (data3 << 12) | (data2 << 8) | (data1 << 4) | data0;
		outputfile << "1 : 0x" << std::hex << std::setfill('0') << std::setw(4) << tmp << std::endl;
		cnt_add0++;
	}

	std::cerr << cnt_add0 << std::endl;
	
	outputfile.close();
	  return;  
}

void NSWConverter::MakeTestLUT(const std::string &outputdir){

    // for NSW pos
	int pt_pos[19][8192] = {};
    int pt_pos_tot[19*8192] = {};
    int cnt_add0 = 0;
    std::string outputfilename = outputdir + "testLUT_NSW_pos.txt";
    std::ofstream outputfile((outputfilename).c_str());
    
    for(int ssc=0;ssc<19;ssc++){
        int cnt = 0;
        int flg = 1;
        for(int addr=0;addr<8192;addr++){
            pt_pos[ssc][addr] = cnt;
            cnt += flg;
            if(cnt == 0 || cnt == 15){
                flg *= -1;
            }
            //pt_pos[ssc][addr] = (ssc+1)%16;
        }
    }

    for(int ssc=0;ssc<19;ssc++){
          for(int address=0; address<8192; address+=4){
        	//pt_pos_tot[ssc*8192 + address] = pt_pos[ssc][address];
            int data0 = pt_pos[ssc][address    ];
            int data1 = pt_pos[ssc][address + 1];
            int data2 = pt_pos[ssc][address + 2];
            int data3 = pt_pos[ssc][address + 3];
            uint16_t tmp = (data3 << 12) | (data2 << 8) | (data1 << 4) | data0;
        	outputfile << "1 : 0x" << std::hex << std::setfill('0') << std::setw(4) << tmp << std::endl;
        	cnt_add0++;
          }
    }


    //for(int i=0;i<19*8192;i+=4){
    //	int data0 = pt_pos_tot[i    ];
    //	int data1 = pt_pos_tot[i + 1];
    //	int data2 = pt_pos_tot[i + 2];
    //	int data3 = pt_pos_tot[i + 3];
    //      	uint16_t tmp = (data3 << 12) | (data2 << 8) | (data1 << 4) | data0;
    //	outputfile << "1 : 0x" << std::hex << std::setfill('0') << std::setw(4) << tmp << std::endl;
    //	cnt_add0++;
    //}

    std::cerr << cnt_add0 << std::endl;

    // NSW angle
	int pt_ang[19][16384] = {};
    int pt_ang_tot[19*16384] = {};
    std::string outputfilename2 = outputdir + "testLUT_NSW_ang.txt";
    std::ofstream outputfile2((outputfilename2).c_str());
    
    for(int ssc=0;ssc<19;ssc++){
        int cnt = 0;
        int flg = 1;
        for(int addr=0;addr<16384;addr++){
            pt_ang[ssc][addr] = cnt;
            cnt += flg;
            if(cnt == 0 || cnt == 15){
                flg *= -1;
            }
            //pt_ang[ssc][addr] = (ssc+1)%16;
        }
    }

    //for(int ssc=0;ssc<19;ssc++){
    //      for(int address=0; address<16384; address++){
    //	pt_ang_tot[ssc*16384 + address] = pt_ang[ssc][address];
    //      }
    //}
    
    cnt_add0 = 0;

    for(int ssc=0;ssc<19;ssc++){
          for(int address=0; address<16384; address+=4){
        	//pt_pos_tot[ssc*8192 + address] = pt_pos[ssc][address];
            int data0 = pt_ang[ssc][address    ];
            int data1 = pt_ang[ssc][address + 1];
            int data2 = pt_ang[ssc][address + 2];
            int data3 = pt_ang[ssc][address + 3];
            uint16_t tmp = (data3 << 12) | (data2 << 8) | (data1 << 4) | data0;
        	outputfile2 << "1 : 0x" << std::hex << std::setfill('0') << std::setw(4) << tmp << std::endl;
        	cnt_add0++;
          }
    }

    //for(int i=0;i<19*16384;i+=4){
    //	int data0 = pt_ang_tot[i    ];
    //	int data1 = pt_ang_tot[i + 1];
    //	int data2 = pt_ang_tot[i + 2];
    //	int data3 = pt_ang_tot[i + 3];
    //      	uint16_t tmp = (data3 << 12) | (data2 << 8) | (data1 << 4) | data0;
    //	outputfile2 << "1 : 0x" << std::hex << std::setfill('0') << std::setw(4) << tmp << std::endl;
    //	cnt_add0++;
    //}
    std::cerr << cnt_add0 << std::endl;
    
    outputfile2.close();
    return;  
}

void NSWConverter::MakeCoeFile(const std::string &outputdir){
    const int LUT_pos_depth = 8192;
    const int LUT_ang_depth = 16384;
    const int write_length = 10; // how many data in ONE line of .coe file
    int pt[4096] = {};
    int pt_pos[LUT_pos_depth] = {};
    int pt_ang[LUT_ang_depth] = {};

    for(int dphi = 0; dphi < 16; dphi++){
        for(int deta = 0; deta < 64; deta++){
           for(int Phi_pos=0;Phi_pos<4;Phi_pos++){
                for(int R_pos=0;R_pos<2;R_pos++){
                    int address = deta + dphi*64 + Phi_pos*1024 + R_pos*4096;
                    pt_pos[address] = TestPatternPos(dphi, deta);
                }
            }
        }
    }

    std::string filename = outputdir + "testLUT_NSW_pos.coe";
    std::ofstream ofs(filename.c_str());
    ofs << "; .coe file for NSW_POS LUT. " << std::endl;
    ofs << "; LUT depth = 8192, LUT width = 4bit" << std::endl;
    ofs << "memory_initialization_radix=16;" << std::endl;
    ofs << "memory_initialization_vector=" << std::endl;
    for(int addr=0;addr<LUT_pos_depth;addr++){
        ofs << std::hex << pt_pos[addr];
        if(addr != LUT_pos_depth-1) ofs << "," << std::endl;
        else                  ofs << ";" << std::endl;
    }

    for(int dtheta = 0; dtheta < 32; dtheta++){
        for(int deta = 0; deta < 64; deta++){
           for(int Phi_pos=0;Phi_pos<4;Phi_pos++){
                for(int R_pos=0;R_pos<2;R_pos++){
                    int address = deta + dtheta*64 + Phi_pos*2048 + R_pos*8192;
                    pt_ang[address] = TestPatternAng(dtheta, deta);
                }
            }
        }
    }

    std::string filename_ang = outputdir + "testLUT_NSW_ang.coe";
    std::ofstream ofs_ang(filename_ang.c_str());
    ofs_ang << "; .coe file for NSW_ANGLE LUT. " << std::endl;
    ofs_ang << "; LUT depth = 16384, LUT width = 4bit" << std::endl;
    ofs_ang << "memory_initialization_radix=16;" << std::endl;
    ofs_ang << "memory_initialization_vector=" << std::endl;
    for(int addr=0;addr<LUT_ang_depth;addr++){
        ofs_ang << std::hex << pt_ang[addr];
        if(addr != LUT_ang_depth-1) ofs_ang << "," << std::endl;
        else                  ofs_ang << ";" << std::endl;
    }

    return;
}

int NSWConverter::TestPatternPos(int dphi, int deta){
    if( (6 <= dphi && dphi <= 9 )   && (26 <= deta && deta <= 38 ) ) return 8;
    if( (3  <= dphi && dphi <= 12 ) && (13 <= deta && deta <= 51 ) ) return 4;
    return 2;
}

int NSWConverter::TestPatternAng(int dtheta, int deta){
    if( (12<= dtheta && dtheta <= 20 ) && (26 <= deta && deta <= 38 ) ) return 8;
    if( (4 <= dtheta && dtheta <= 28)  && (13 <= deta && deta <= 51 ) ) return 4;
    return 2;
}
